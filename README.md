## Golang Docker image ##
A container to run Golang on x64 architecture. This image is derivated from the official `golang 1.7` image with some optimizations:

- [https://hub.docker.com/_/golang/](https://hub.docker.com/_/golang/)
- [https://github.com/docker-library/golang/](https://github.com/docker-library/golang/)

### Git branch & Docker tags ###

- `latest` => `1.7` / branch master
- `1.7` / branch 1.7
- `1.6.3` / branch 1.6.3
- `1.5.4` / branch 1.5.4
- `1.4.3` / branch 1.4.3
- `1.3.3` / branch 1.3.3
- `1.2.2` / branch 1.2.2

### Image build & optimization ###

This image is "one layer image", all `RUN` commands was group into one in order to reduce the final image size.

Moreover, every `ADD` or `COPY` commands point on an external URL instead of local files. So if you want to rebuild the image, it's not necessary to pull any git repository before, you could just do:

    docker build -t golang:1.7 https://bitbucket.org/jtof_fap/golang/raw/master/Dockerfile

### Image automatic updates ###

This image is automatically rebuilt when:

- When base image is updated :

This image is based on an up-to-date debian image an is automatically rebuild when base image is updated. For more information about this process, reports to: [https://hub.docker.com/r/insecurity/debian/](https://hub.docker.com/r/insecurity/debian/)

- When git repository is updated ([https://bitbucket.org/jtof_fap/golang](https://bitbucket.org/jtof_fap/golang)):

### Usage and documentation ###

Reports to the official `Golang` repository:

- [https://hub.docker.com/_/golang/](https://hub.docker.com/_/golang/)

